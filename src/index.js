// Import the react React and ReactDOM libraries
import React from "react";
import ReactDOM from "react-dom";

const buttonStyle = {
    backgroundColor: "blue",
    color: "white"
}

// Create a React component
const App = () => {
    const buttonText = "Click Me!";
    const labelText = "Enter Name: ";

    return (
        <div>
            <label className="label" htmlFor="name">
                {labelText}
            </label>
            <input id="name" type="text" />
            <button style={buttonStyle}>
                {buttonText}
            </button>
        </div>
    )
};

// Take the React Componet and show it on the screen

ReactDOM.render(
    <App />,
    document.querySelector("#root")
);
